package mx.com.gm.domain;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/*  Nos permite generar el código con los método básicos de la clase */
@Data
@Entity
@Table(name = "persona")
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idPersona;
    private String nombre;
    private String apellido;
    private String email;
    private String telefono;
}
